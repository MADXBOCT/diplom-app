FROM centos:7
RUN yum install python3 python3-pip -y
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN mkdir /python_web
COPY python-web.py /python_web/python-web.py
EXPOSE 80
CMD ["python3", "/python_web/python-web.py"]
